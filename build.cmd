@echo off

echo ### start new build ###

REM cleanup previous build
echo (1/4) remove previous build files/folders
if exist dist rmdir dist /s /q

REM copy node_modules
echo (2/4) copy node_modules
mkdir dist
xcopy node_modules dist\node_modules /v /r /s /y /i /q

REM build app via npm build-task
echo (3/4) compile ts-files
npm run build

REM zip
echo (4/4) zip
tar -cvzf dist\node_backend.tar.gz dist

echo ### build finished ###
