3rd party libraries
======

- Web API: https://expressjs.com/de/

- Input validation: https://www.npmjs.com/package/class-validator

- DI: https://www.npmjs.com/package/awilix & https://www.npmjs.com/package/awilix-express

- ORM: https://typeorm.io/

- Database (DEV): https://sql.js.org/documentation/

- Database (PROD): https://www.npmjs.com/package/oracledb

- Logger: https://www.npmjs.com/package//winston

- LDAP: http://ldapjs.org/