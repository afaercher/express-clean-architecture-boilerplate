import { IsString, MaxLength, MinLength, IsInt, Min, Max, IsBoolean, IsEmail } from 'class-validator';
import { Request } from 'express';
import { BaseDto } from './base/base.dto';

export class TicketDto extends BaseDto {
  @IsString()
  @MinLength(1)
  @MaxLength(8)
  public pc: string;

  @IsString()
  @MinLength(10)
  @MaxLength(10)
  public validFrom: string;

  @IsString()
  @MinLength(10)
  @MaxLength(10)
  public validUntil: string;

  @IsInt()
  @Min(2)
  @Max(9999)
  public duration: number;

  @IsInt()
  @Min(-1)
  @Max(9999)
  public reboots: number;

  @IsBoolean()
  public intrusionProtection: boolean;

  @IsBoolean()
  public autoReboot: boolean;

  @IsBoolean()
  public usbServiceMode: boolean;

  @IsBoolean()
  public disableTimeAutomationAlert: boolean;

  @IsString()
  @IsEmail()
  public issuer: string;

  private constructor(pc: string, validFrom: string, validUntil: string, duration: number, reboots: number, intrusionProtection: boolean, autoReboot: boolean, usbServiceMode: boolean, disableTimeAutomationAlert:boolean, issuer: string) {
    super();
    this.pc = pc;
    this.validFrom = validFrom;
    this.validUntil = validUntil;
    this.duration = duration;
    this.reboots = reboots;
    this.intrusionProtection = intrusionProtection;
    this.autoReboot = autoReboot;
    this.usbServiceMode = usbServiceMode;
    this.disableTimeAutomationAlert = disableTimeAutomationAlert;
    this.issuer = issuer;
  }

  public static async create(req: Request): Promise<TicketDto> {
    var dto = new TicketDto(
        req.body.pc as string,
        req.body.validFrom as string,
        req.body.validUntil as string,
        req.body.duration as number,
        req.body.reboots as number,
        req.body.intrusionProtection as boolean,
        req.body.autoReboot as boolean,
        req.body.usbServiceMode as boolean,
        req.body.disableTimeAutomationAlert as boolean,
        req.headers.remote_user as string
      );

      await dto.validate();
      return dto;
  }
}   