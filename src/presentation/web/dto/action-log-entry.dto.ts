import { IsString, MaxLength, MinLength, IsEmail } from 'class-validator';
import { Request } from 'express';
import { BaseDto } from './base/base.dto';

export class ActionLogEntryDto extends BaseDto {
  @IsString()
  @IsEmail()
  @MinLength(1)
  @MaxLength(100)
  public user: string;

  @IsString()
  @MinLength(1)
  @MaxLength(100)
  public service: string;

  public uuid: string;

  private constructor(service: string, user: string, uuid: string) {
    super();
    this.user = user;
    this.service = service
    this.uuid = uuid;
  }

  public static async create(req: Request): Promise<ActionLogEntryDto> {
    var dto = new ActionLogEntryDto(req.body.service, req.headers['remote_user'] as string, req.body.uuid);

    await dto.validate();
      return dto;
  }
}