import { ValidationException } from '../../../../application/exceptions/validation.exception';
import { validate, ValidatorOptions } from 'class-validator';

export abstract class BaseDto {
  
  protected constructor() { }

  protected async validate(): Promise<void> {

    var validatorOptions: ValidatorOptions = {
      skipMissingProperties: false,
      stopAtFirstError: false
    }

    var errors = await validate(this, validatorOptions);
    if (errors.length > 0) {
      throw new ValidationException('invalid data', JSON.stringify(errors));
    }
  }
}