export class HdeAgent {
  public rowNumber: number;

  public name: string;
  public nodeState: string;
  public installState: string;
  public version: string;
  public statusSentAt: Date;
  public profileReceivedAt: Date;
  public profileTimestamp: Date;
  public hasDifferences: string;
  public encrypted: string;
  public wakeOnLan: string;
  public onlineHelpdesk: string;
  public offlineHelpdesk: string;
  public dr2CentralAvailable: string;
  public dr2InFolderAvailable: string;
  public lastUser: string;
  public userCount: string;
  public path: string;
  public reportedBy: string;
  public serverListTimestamp: Date;
  public compliance: string;
  public secureEraseState: string;
  public encryptionState: string;
  public encryptionPercent: string;
  public encryptionStartedAt: Date;
  public encryptionFinishedAt: Date;
  public certificateAvailable: string;
  public certNotValidAfter: string;

  public wsid: string;
  public description: string;
  public mac: string;
  public deviceType: string;

  public constructor(rowNumber: number) {
    this.rowNumber = rowNumber;
  }
}
