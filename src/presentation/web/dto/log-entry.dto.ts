import { IsString, MaxLength, MinLength } from 'class-validator';
import { Request } from 'express';
import { BaseDto } from './base/base.dto';

export class LogEntryDto extends BaseDto {
  @IsString()
  @MinLength(1)
  @MaxLength(99999)
  public value: string;

  @IsString()
  @MinLength(1)
  @MaxLength(20)
  public level: string;

  public actionUuid: string;

  private constructor(value: string, level: string, actionUuid: string) {
    super();
    this.value = value;
    this.level = level;
    this.actionUuid = actionUuid;
  }

  public static async create(req: Request): Promise<LogEntryDto> {
    var dto = new LogEntryDto(req.body.value, req.body.level, req.body.actionUuid);
    await dto.validate();
    return dto;
  }
}