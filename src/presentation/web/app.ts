import { serverLog } from '../../tools/logger';
import { environment } from '../../environments/environment';
import { getAppDataSource } from '../../infrastructure/database/datasource/datasource';

import http from 'http';

import { AddressInfo } from 'net';
import { createServer, registerErrorHandler } from './express-settings'

import { loadContainer } from './container';

import 'reflect-metadata' // needed for typeorm

const port = environment.port;
const host = environment.host;

// todo: init database
const initDatabase = () => {
  var datasource = getAppDataSource();
  datasource.initialize()
  .then(() => {
      startServer();
  })
  .catch((error) => console.log(error))
}

const startServer = async() => {
    const app = await createServer();
    
    loadContainer(app);

    registerErrorHandler(app);

    //debugRegisteredRoutes(app);

    const server = http.createServer(app).listen({host, port}, () => {
        const addressInfo = server.address() as AddressInfo;
        serverLog.info(`Automation backend started. Listening on ${addressInfo.address}:${addressInfo.port}.`);
    });

    const signalTraps: NodeJS.Signals[] = ['SIGTERM', 'SIGINT', 'SIGUSR2'];
    signalTraps.forEach((type) => {
        process.once(type, async() => {
            console.log(`interrupt ${type}`);

            server.close(() => {
                console.log('Automation backend stopped.');
            });
        });
    });
};

initDatabase();

function debugRegisteredRoutes(app){
  function print (path, layer) {
    console.log(path)
    console.log(layer)
    if (layer.route) {
      layer.route.stack.forEach(print.bind(null, path.concat(split(layer.route.path))))
    } else if (layer.name === 'router' && layer.handle.stack) {
      layer.handle.stack.forEach(print.bind(null, path.concat(split(layer.regexp))))
    } else if (layer.method) {
      console.log('%s /%s',
        layer.method.toUpperCase(),
        path.concat(split(layer.regexp)).filter(Boolean).join('/'))
    }
  }
  
  function split (thing) {
    if (typeof thing === 'string') {
      return thing.split('/')
    } else if (thing.fast_slash) {
      return ''
    } else {
        console.log('thing: ',thing);
      var match = thing.toString()
        .replace('\\/?', '')
        .replace('(?=\\/|$)', '$')
        .match(/^\/\^((?:\\[.*+?^${}()|[\]\\\/]|[^.*+?^${}()|[\]\\\/])*)\$\//)
      return match
        ? match[1].replace(/\\(.)/g, '$1').split('/')
        : '<complex:' + thing.toString() + '>'
    }
  }
  
  app._router.stack.forEach(print.bind(null, []))
}
