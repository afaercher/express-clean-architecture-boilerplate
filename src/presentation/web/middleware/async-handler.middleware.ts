const _asyncHandler = (routeAction) => {
  return async (req, res, next) => {
    try {
      await routeAction(req, res);
    } catch (err) {
      next(err);
    }
  };
};

export const asyncHandler = async (routeAction, req, res, next) => {
  var fnc = _asyncHandler(routeAction);
  return fnc(req, res, next);
}