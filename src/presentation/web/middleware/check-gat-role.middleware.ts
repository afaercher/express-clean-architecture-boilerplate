import { Request, Response, NextFunction } from 'express';
import { environment } from '../../../environments/environment';
import ldap, { ClientOptions, SearchEntry } from 'ldapjs';
import { serverLog } from '../../../tools/logger';

export const checkGAT = (gatDomains: Array<string>) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        let client: ldap.Client | undefined;

        try {
            const email = res.locals.remoteUser;
            client = ldap.createClient(ldapClientOptions);
            
            let connectionError = false;
            let aborted = false;
    
            client.on('error', (err) => {

                if (aborted) {
                    console.log('already canceled');
                    return;
                }

                console.log('error:', err);

                connectionError = true;
                aborted = true;
                // console.log('connection err', err);
                
                res.status(403).send('not permitted!!!!');
                
                if (client !== undefined && client.connected) {
                    client.destroy();
                }
                
                return;
            })
    
            const opts = {
              filter: "(mail=" + email + ")",
              scope: "sub" as "base" | "one" | "sub" | undefined,
              attributes: ["memberof"]
            };
            let isEntitled = false;
    
    
            if (connectionError) {
                return;
            }

            client.search('ou=people,ou=global,dc=dbgroup,dc=com', opts, (err, resLdap) => {
                // console.log('search res');
            
                if (err) {
                    console.log('search err', err);
                    if (!aborted) {
                        res.status(403).send('not permitted');
                    }
                    return;
                }
    
                resLdap.on('searchEntry', (entry: SearchEntry) => {
                    const gatDomainsList = (entry.object.memberof as string[]).map(entry => {
                        const gatDomainName = getGatDomainFromMemberOfString(entry)
                        return gatDomainName ?  gatDomainName : undefined;
                    });
                    for (const gatDomain of gatDomains) {
                        if (gatDomainsList.includes(gatDomain)) {
                            isEntitled = true;
                            break;
                        }
                    }
                });
                resLdap.on('error', (err) => {
                    serverLog.error(err.message);
                });
                resLdap.on('end', (result) => {
                    if (client !== undefined && client.connected) {
                        client.destroy();
                        // console.log('ldap connection closed', client)
                    }
                    if (isEntitled) {
                        serverLog.info('the user was successfully identified')
                        next();
                        return;
                    }
                    serverLog.error('the user ' + email + ' has insufficient permissions --> access denied')
                    if (!aborted) {
                        res.status(403).send('not permitted');
                    }
                });
            });
        }
        catch (ex) {
            console.log('exception', ex);
            res.status(500).send('ldap exception');
        }
    }
} 

const ldapClientOptions: ClientOptions = {
    url: environment.ldaps_url,
    tlsOptions: { 'rejectUnauthorized': false },
    bindDN: environment.ldap_bindDN,
    bindCredentials: environment.ldap_bindCredentials
} 

function getGatDomainFromMemberOfString(memberOf: string): string | null {
    const matches = memberOf.match('^cn=[a-zA-Z_\\-0-9]*');
    if (!matches || matches.length === 0) {
        return null;
    }
    return matches[0].split('=')[1];
}