import { Request, Response, NextFunction } from 'express';
import { serverLog } from '../../../tools/logger';

export const checkIp = (validIps: Array<string>) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        try {
            const requestIp = req.socket.remoteAddress;
            if (!requestIp) {
                res.sendStatus(403);
                return;
            }

            if (validIps.find(x => x === requestIp)) {
                next();
                return;
            }
            
            
            res.sendStatus(403);
        }
        catch (ex) {
            console.log('exception', ex);
            res.status(500).send('ip exception');
        }
    }
} 
