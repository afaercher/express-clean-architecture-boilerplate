import { serverLog } from '../../../tools/logger';
import { NextFunction, Request, Response } from 'express';

export const checkRemoteUser = async (req: Request, res: Response, next: NextFunction) => {
    const userEmail = req.headers['remote_user'] as string;
    serverLog.debug('checkRemoteUser middleware was called');
    if (!userEmail) {
        serverLog.warn('checkRemoteUser(): no user email was provided --> access denied');
        res.status(401).send('not authorized');
        return;
    }
    res.locals.remoteUser = userEmail;
    next();
}