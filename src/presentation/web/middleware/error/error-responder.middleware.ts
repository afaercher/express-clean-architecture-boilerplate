import { Request, Response, NextFunction } from "express";
import { BaseException } from "../../../../application/exceptions/base/base.exception";

export const errorResponder = (
  error: BaseException,
  request: Request,
  response: Response,
  next: NextFunction
) => {

  if (!(error instanceof BaseException)) {
    response.status(500).send('Unexpected error');
    return;
  }

  response.status(500).send(error.message);
};