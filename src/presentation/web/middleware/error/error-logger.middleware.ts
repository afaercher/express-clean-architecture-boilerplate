import { Request, Response, NextFunction } from "express";
import { BaseException } from "../../../../application/exceptions/base/base.exception";
import { serverLog } from '../../../../tools/logger';

export const errorLogger = (
  error: BaseException,
  request: Request,
  response: Response,
  next: NextFunction
) => {
  if (error instanceof BaseException) {
    serverLog.error(`Exception: ${error}`);
  } else {
    serverLog.error(`Unexpexted exception: ${error}`);
  }

  next(error);
};