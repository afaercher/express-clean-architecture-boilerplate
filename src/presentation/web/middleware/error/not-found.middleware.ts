import { Request, Response, NextFunction } from "express";

export const notFoundHandler = (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  console.log('ERROR NOT FOUND');
  const message = "Resource not found";

  response.status(404).send(message);
};