import { TicketDto } from "../dto/ticket.dto";
import { Ticket } from "../../../domain/models/ticket.model";

export const mapDtoToModel = (dto: TicketDto): Ticket => {
  var model = new Ticket(
    dto.pc,
    dto.validFrom,
    dto.validUntil,
    dto.duration,
    dto.reboots,
    dto.intrusionProtection,
    dto.autoReboot,
    dto.usbServiceMode,
    dto.disableTimeAutomationAlert,
    dto.issuer
  );
  
  return model;
}