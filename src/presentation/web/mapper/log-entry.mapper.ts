import { LogEntry } from "../../../domain/models/log-entry.model"
import { LogEntryDto } from "../dto/log-entry.dto"
import { ActionLogEntry } from "../../../domain/models/action-log-entry.model";

export const mapDtoToModel = (dto: LogEntryDto): LogEntry => {
  var model = new LogEntry();
  model.value = dto.value;
  model.level = dto.level;
  model.actionUuid = dto.actionUuid;
  model.actionLogEntry = new ActionLogEntry();
  model.actionLogEntry.uuid = dto.actionUuid;

  return model;
}