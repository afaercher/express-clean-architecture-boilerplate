import { ActionLogEntry } from "../../../domain/models/action-log-entry.model";
import { ActionLogEntryDto } from "../dto/action-log-entry.dto";

export const mapDtoToModel = (dto: ActionLogEntryDto): ActionLogEntry => {
  var model = new ActionLogEntry();
  model.user = dto.user;
  model.service = dto.service;
  model.uuid = dto.uuid;
  return model;
}