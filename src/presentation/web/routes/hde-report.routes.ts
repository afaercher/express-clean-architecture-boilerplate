import { checkRemoteUser } from '../middleware/check-remote-user.middleware';
import { environment } from '../../../environments/environment';
import { checkGAT } from '../middleware/check-gat-role.middleware';
import { checkIp } from '../middleware/check-ip.middleware';
import { HdeReportController } from '../controller/hde-report.controller';
import { route, GET, POST, before } from 'awilix-express'
import { asyncHandler } from '../middleware/async-handler.middleware';

@route('/hdereport')
@route('//hdereport')
export default class HdeReportApi{
  
  private _controller: HdeReportController;

  constructor(cradle) {
    this._controller = new HdeReportController(); // todo
  }

  @POST()
  @before([checkIp(environment.validIps), checkRemoteUser, checkGAT([environment.gat_role_ticket_creator])])
  async create(req, res, next) {
    await asyncHandler(HdeReportController.create, req, res, next);
  }
  
  @route('/agents')
  @POST()
  @before([checkIp(environment.validIps), checkRemoteUser, checkGAT([environment.gat_role_ticket_creator])])
  async create2(req, res, next) {
    await asyncHandler(HdeReportController.create, req, res, next);
  }
}