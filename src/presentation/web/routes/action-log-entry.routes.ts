import { checkRemoteUser } from '../middleware/check-remote-user.middleware';
import { checkIp } from '../middleware/check-ip.middleware';
import { environment } from '../../../environments/environment';
import { ActionLogEntryController } from '../controller/action-log-entry.controller';
import { route, GET, POST, before } from 'awilix-express'
import { LogEntryController } from '../controller/log-entry.controller';
import { asyncHandler } from '../middleware/async-handler.middleware';

@route('/actionlogentry')
@route('//actionlogentry')
export default class ActionLogEntryApi{
  
  private _actionLogEntryController: ActionLogEntryController;
  private _logEntryController: LogEntryController;

  constructor(cradle) {
    this._actionLogEntryController = cradle.actionLogEntryController;
    this._logEntryController = cradle.logEntryController;
  }

  @GET()
  @before([checkIp(environment.validIps), checkRemoteUser])
  async all(req, res, next) {
    await asyncHandler(this._actionLogEntryController.all, req, res, next);
  }

  @route('/:id/logentry')
  @GET()
  @before([checkIp(environment.validIps), checkRemoteUser])
  async allLogEntries(req, res, next) {
    await asyncHandler(this._logEntryController.allByActionLogEntryId, req, res, next);
  }

  @POST()
  @before([checkIp(environment.validIps), checkRemoteUser])
  async create(req, res, next) {
    await asyncHandler(this._actionLogEntryController.create, req, res, next);
  }
}
