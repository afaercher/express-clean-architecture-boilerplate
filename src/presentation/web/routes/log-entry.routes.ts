import { checkRemoteUser } from '../middleware/check-remote-user.middleware';
import { checkIp } from '../middleware/check-ip.middleware';
import { environment } from '../../../environments/environment';
import { LogEntryController } from '../controller/log-entry.controller';
import { route, GET, POST, before } from 'awilix-express'
import { asyncHandler } from '../middleware/async-handler.middleware';

@route('/logentry')
@route('//logentry')
export default class LogEntryApi{
  
  private _controller: LogEntryController;

  constructor(cradle) {
    this._controller = cradle.logEntryController;
  }

  @GET()
  @before([checkIp(environment.validIps), checkRemoteUser])
  async all(req, res, next) {
    await asyncHandler(this._controller.all, req, res, next);
  }

  @POST()
  @before([checkIp(environment.validIps), checkRemoteUser])
  async create(req, res, next) {
    await asyncHandler(this._controller.create, req, res ,next);
  }
}
