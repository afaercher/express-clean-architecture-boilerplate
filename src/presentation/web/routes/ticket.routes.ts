import { checkRemoteUser } from '../middleware/check-remote-user.middleware';
import { TicketController } from '../controller/ticket.controller';
import { checkGAT } from '../middleware/check-gat-role.middleware';
import { checkIp } from '../middleware/check-ip.middleware';
import { environment } from '../../../environments/environment';
import { route, GET, POST, before } from 'awilix-express'
import { asyncHandler } from '../middleware/async-handler.middleware';

@route('/ticket')
@route('//ticket')
export default class TicketApi{
  
  private _controller: TicketController;

  constructor(cradle) {
    this._controller = cradle.ticketController;
  }

  @POST()
  @before([checkIp(environment.validIps), checkRemoteUser, checkGAT([environment.gat_role_ticket_creator])])
  async create(req, res, next) {
    await asyncHandler(this._controller.create, req, res, next);
  }

  @route('/create')
  @POST()
  @before([checkIp(environment.validIps), checkRemoteUser, checkGAT([environment.gat_role_ticket_creator])])
  async create2(req, res, next) {
    await asyncHandler(this._controller.create, req, res, next);
  }
}
