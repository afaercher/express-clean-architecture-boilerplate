import express from 'express';
import cors from 'cors';
import helmet from "helmet";
import { errorLogger } from './middleware/error/error-logger.middleware';
import { errorResponder } from './middleware/error/error-responder.middleware';
import { notFoundHandler } from './middleware/error/not-found.middleware';

const createServer = (): express.Application => {
    const app = express();

    app.use(express.urlencoded({ extended: true }));
    app.use(helmet())
    app.use(cors());
    app.use(express.json());
    return app;
};

const registerErrorHandler = (app: express.Application) => {
    app.use(errorLogger);
    app.use(errorResponder);
    app.use(notFoundHandler);
}

export { createServer, registerErrorHandler };
