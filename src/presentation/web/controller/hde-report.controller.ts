import { serverLog } from '../../../tools/logger';
import { Response } from 'express';
import { Request } from 'express';
import { spawn } from 'child_process';
import * as fs from 'fs';
import { join } from 'path';
import { environment } from '../../../environments/environment';
import { hostname } from 'os';
import { HdeAgent } from '../dto/hde-agent.dto';
import { DateTime } from 'luxon';

// todo
export class HdeReportController {

    private static hdeReport = 1;
    private static devices = 2;

    private static hdeReportColumnNoName = 0;
    private static hdeReportColumnNoNodeState = 1;
    private static hdeReportColumnNoInstallState = 2;
    private static hdeReportColumnNoVersion = 3;
    private static hdeReportColumnNoStatusSentAt = 4;
    private static hdeReportColumnNoProfileReceivedAt = 5;
    private static hdeReportColumnNoProfileTimestamp = 6;
    private static hdeReportColumnNoHasDifferences = 7;
    private static hdeReportColumnNoEncrypted = 8;
    private static hdeReportColumnNoWakeOnLan = 9;
    private static hdeReportColumnNoOnlineHelpdesk = 10;
    private static hdeReportColumnNoOfflineHelpdesk = 11;
    private static hdeReportColumnNoDr2CentralAvailable = 12;
    private static hdeReportColumnNoDr2InFolderAvailable = 13;
    private static hdeReportColumnNoLastUser = 14;
    private static hdeReportColumnNoUserCount = 15;
    private static hdeReportColumnNoPath = 16;
    private static hdeReportColumnNoReportedBy = 17;
    private static hdeReportColumnNoServerListTimestamp = 18;
    private static hdeReportColumnNoCompliance = 19;
    private static hdeReportColumnNoSecureEraseState = 20;
    private static hdeReportColumnNoEncryptionState = 21;
    private static hdeReportColumnNoEncryptionPercent = 22;
    private static hdeReportColumnNoEncryptionStartedAt = 23;
    private static hdeReportColumnNoEncryptionFinishedAt = 24;
    private static hdeReportColumnNo8021XCertificateAvailable = 25;
    private static hdeReportColumnNo8021XCertNotValidAfter = 26;

    private static deviceColumnNoHostname = 0;
    private static deviceColumnNoWsid = 1;
    private static deviceColumnNoMac = 2;
    private static deviceColumnNoDescription = 3;
    private static deviceColumnNoDeviceType = 4;
    
    static create = async (req: Request, res: Response) => {
        const contentHdeReport = HdeReportController.readFile(environment.hde_report);
        const agents1 = HdeReportController.parseContent(contentHdeReport, HdeReportController.hdeReport);

        const contentDevices = HdeReportController.readFile(environment.hde_devices);
        const agents2 = HdeReportController.parseContent(contentDevices, HdeReportController.devices);

        // merge agents & agents2
        for (const agent1 of agents1) {
            const agent2 = agents2.find(x => x.name === agent1.name);
            if (!agent2) {
                continue;
            }

            agent1.wsid = agent2.wsid;
            agent1.deviceType = agent2.deviceType;
            agent1.description = agent2.description;
        }

        res.send(agents1);       
    }

    public static readFile(filePath: string): string[] {
       return fs.readFileSync(filePath, 'utf8').split(/\r\n|\r|\n/);
    }

    public static parseContent(content: string[], modus: number): HdeAgent[] {
        const agents: HdeAgent[] = [];
        var isHeaderLine = true;

        var no = 0;
        for (const line of content) {
            if (isHeaderLine) {
                isHeaderLine = false;
                continue;
            }

            no++;

            if (modus === this.hdeReport) {
                const agent = this.parseLineHdeReport(line, no);
                if (agent) {
                    agents.push(agent);
                }
                continue;
            }
            if (modus === this.devices) {
                const agent = this.parseLineDevices(line, no);
                if (agent) {
                    agents.push(agent);
                }
            }
            
        }
        
        return agents;
    }

    public static parseLineHdeReport(line: string, lineNo: number): HdeAgent {
        if (!line || line.length <= 0) {
            return null;
        }

        const array = line.split(',');

        const agent = new HdeAgent(lineNo);
        agent.name = array[this.hdeReportColumnNoName];
        agent.nodeState = array[this.hdeReportColumnNoNodeState];
        agent.installState = array[this.hdeReportColumnNoInstallState];
        agent.version = array[this.hdeReportColumnNoVersion];
        agent.statusSentAt = HdeReportController.parseDateValue(array[this.hdeReportColumnNoStatusSentAt]);
        agent.profileReceivedAt = HdeReportController.parseDateValue(array[this.hdeReportColumnNoProfileReceivedAt]);
        agent.profileTimestamp =  HdeReportController.parseDateValue(array[this.hdeReportColumnNoProfileTimestamp]);
        agent.hasDifferences = array[this.hdeReportColumnNoHasDifferences];
        agent.encrypted = array[this.hdeReportColumnNoEncrypted];
        agent.wakeOnLan = array[this.hdeReportColumnNoWakeOnLan];
        agent.onlineHelpdesk = array[this.hdeReportColumnNoOnlineHelpdesk];
        agent.offlineHelpdesk = array[this.hdeReportColumnNoOfflineHelpdesk];
        agent.dr2CentralAvailable = array[this.hdeReportColumnNoDr2CentralAvailable];
        agent.dr2InFolderAvailable = array[this.hdeReportColumnNoDr2InFolderAvailable];
        agent.lastUser = array[this.hdeReportColumnNoLastUser];
        agent.userCount = array[this.hdeReportColumnNoUserCount];
        agent.path = array[this.hdeReportColumnNoPath];
        agent.reportedBy = array[this.hdeReportColumnNoReportedBy];
        agent.serverListTimestamp =  HdeReportController.parseDateValue(array[this.hdeReportColumnNoServerListTimestamp]);
        agent.compliance = array[this.hdeReportColumnNoCompliance];
        agent.secureEraseState = array[this.hdeReportColumnNoSecureEraseState];
        agent.encryptionState = array[this.hdeReportColumnNoEncryptionState];
        agent.encryptionPercent = array[this.hdeReportColumnNoEncryptionPercent];
        agent.encryptionStartedAt =  HdeReportController.parseDateValue(array[this.hdeReportColumnNoEncryptionStartedAt]);
        agent.encryptionFinishedAt =  HdeReportController.parseDateValue(array[this.hdeReportColumnNoEncryptionFinishedAt]);
        agent.certificateAvailable = array[this.hdeReportColumnNo8021XCertificateAvailable];
        agent.certNotValidAfter = array[this.hdeReportColumnNo8021XCertNotValidAfter];

        agent.name = agent.name.replace('.VIRTUAL', '');

        return agent
    }

    public static parseLineDevices(line: string, lineNo: number): HdeAgent {
        if (!line || line.length <= 0) {
            return null;
        }

        const array = line.split(',');

        const agent = new HdeAgent(lineNo);
        agent.name = array[this.deviceColumnNoHostname].replace(new RegExp('"', 'g'),'');
        agent.wsid = array[this.deviceColumnNoWsid].replace(new RegExp('"', 'g'),'');
        agent.mac = array[this.deviceColumnNoMac].replace(new RegExp('"', 'g'),'');
        agent.description = array[this.deviceColumnNoDescription].replace(new RegExp('"', 'g'),'');
        agent.deviceType = array[this.deviceColumnNoDeviceType].replace(new RegExp('"', 'g'),'');

        return agent;
    }

    private static parseDateValue(str: string): Date {
        const strWithTimezone = str + ' +0';
        return DateTime.fromFormat(strWithTimezone, 'dd.MM.yyyy HH:mm:ss Z').toJSDate();
    }
}