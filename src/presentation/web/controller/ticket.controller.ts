import { Response } from 'express';
import { Request } from 'express';
import { Ticket } from '../../../domain/models/ticket.model';
import { ICreateTicket } from '../../../application/api/ticket/create-ticket.interface';
import * as fs from 'fs';
import { TicketDto } from '../dto/ticket.dto';
import { mapDtoToModel } from '../mapper/ticket.mapper';

export class TicketController {
    private _createUseCase: ICreateTicket;

    constructor(cradle) {
        this._createUseCase = cradle.ticketCreateUseCase;
    }

    public create = async (req: Request, res: Response) => {

        var ticket: Ticket;

        try {
            var dto = await TicketDto.create(req);
            ticket = mapDtoToModel(dto);

            await this._createUseCase.execute(ticket);

            res.setHeader('Content-Type', 'application/zip');
            res.setHeader('Content-Transfer-Encoding', 'Binary');
            res.setHeader('Content-Disposition', 'attachment; filename=ticket.tsx');
            res.download(ticket.filepathTarget, () => {
                fs.rm(ticket.filepathSource, () => {});
                fs.rm(ticket.filepathTarget, () => {});
            });

        } catch (err) {
            if (ticket != null && ticket.filepathSource != null) {
                fs.rm(ticket.filepathSource, () => {});
            }
            if (ticket != null && ticket.filepathTarget != null) {
                fs.rm(ticket.filepathTarget, () => {});
            }
            
            throw err;
        }
    }   
}