import { Response } from 'express';
import { Request } from 'express';
import { ICreateActionLogEntry } from '../../../application/api/log/create-action-log-entry.interface';
import { IGetAllActionLogEntries } from '../../../application/api/log/get-all-action-log-entries.interface';
import { ActionLogEntryDto } from '../dto/action-log-entry.dto';
import { mapDtoToModel } from '../mapper/action-log-entry.mapper';

export class ActionLogEntryController {
    private _createUseCase: ICreateActionLogEntry;
    private _getAllUseCase: IGetAllActionLogEntries;
    
    constructor(cradle) {
        this._createUseCase = cradle.actionLogEntryCreateUseCase;
        this._getAllUseCase = cradle.actionLogEntryGetAllUseCase;
    }

    public all = async (req: Request, res: Response) => {   
        var models = await this._getAllUseCase.execute();
        res.send(models);
    }

    public create = async (req: Request, res: Response) => {   
        var dto = await ActionLogEntryDto.create(req);
        
        var result = await this._createUseCase.execute(mapDtoToModel(dto));
        res.send(result);
    }
}