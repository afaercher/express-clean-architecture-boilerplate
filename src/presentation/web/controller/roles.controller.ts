import { serverLog } from '../../../tools/logger';
import { Response } from 'express';
import { Request } from 'express';
import { environment } from '../../../environments/environment';
import ldap, { ClientOptions, SearchEntry } from 'ldapjs';

// @todo
export class RolesController {
    static get = async (req: Request, res: Response) => {   
        let client: ldap.Client | undefined;

        try {
            if (!req || !req.body) {
                res.sendStatus(400);
                return;
            }

            client = ldap.createClient(ldapClientOptions);
    
            let connectionError = false;
            let aborted = false;

            client.on('error', (err) => {

                if (aborted) {
                    console.log('already canceled');
                    return;
                }

                console.log('error:', err);

                connectionError = true;
                aborted = true;
                console.log('connection err', err);
                        
                if (client !== undefined && client.connected) {
                    client.destroy();
                }
                
                return;
            })

            const opts = {
            filter: '(memberof=cn=Automation_Promotion_PROD,ou=SB_Deployment,ou=groups,ou=services,ou=global,dc=dbgroup,dc=com)',
            scope: "sub" as "base" | "one" | "sub" | undefined,
            attributes: ['uid', 'dn', 'cn', 'mail', 'people', 'user', 'users', 'member', 'members']
            };
            let isEntitled = false;


            if (connectionError) {
                return;
            }

            client.search('ou=people,ou=global,dc=dbgroup,dc=com', opts, (err, resLdap) => {               
                
                console.log('search res');

                //console.log(resLdap);
            
                if (err) {
                    console.log('search err', err);
                    if (!aborted) {
                    console.log('not permitted');
                    }
                    return;
                }

                resLdap.on('searchEntry', (entry: SearchEntry) => {
                console.log(entry.object);
                });
                resLdap.on('error', (err) => {
                console.log(err.message);
                });
                resLdap.on('end', (result) => {
                    if (client !== undefined && client.connected) {
                        client.destroy();
                        // console.log('ldap connection closed', client)
                        console.log('ldap connection closed')
                    }
                    if (isEntitled) {
                        console.log('the user was successfully identified')
                        return;
                    }
                    // console.log('the user ' + email + ' has insufficient permissions --> access denied')
                    // console.log('no permissions - end request');
                    console.log('end request');
                    if (!aborted) {
                    }
                });
            });

        } catch (err) {
            serverLog.error('exception: ' + err);

            res.status(400).send('exception');
        }
    }
}

const ldapClientOptions: ClientOptions = {
    url: environment.ldaps_url,
    tlsOptions: { 'rejectUnauthorized': false },
    bindDN: environment.ldap_bindDN,
    bindCredentials: environment.ldap_bindCredentials
} 

function getGatDomainFromMemberOfString(memberOf: string): string | null {
    const matches = memberOf.match('^cn=[a-zA-Z_\\-0-9]*');
    if (!matches || matches.length === 0) {
        return null;
    }
    return matches[0].split('=')[1];
}