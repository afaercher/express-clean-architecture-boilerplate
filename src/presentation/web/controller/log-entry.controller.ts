import { Response } from 'express';
import { Request } from 'express';
import { IGetAllLogEntries } from '../../../application/api/log/get-all-log-entries.interface';
import { ICreateLogEntry } from '../../../application/api/log/create-log-entry.interface';
import { IGetAllLogEntriesByActionLogEntryId } from '../../../application/api/log/get-all-log-entries-by-action-log-entry-id.interface';
import { LogEntryDto } from '../dto/log-entry.dto';
import { mapDtoToModel } from '../mapper/log-entry.mapper';

export class LogEntryController {
    private _createUseCase: ICreateLogEntry;
    private _getAllUseCase: IGetAllLogEntries;
    private _getAllByActionLogEntryIdUseCase: IGetAllLogEntriesByActionLogEntryId;

    constructor(cradle) {
        this._createUseCase = cradle.logEntryCreateUseCase;
        this._getAllUseCase = cradle.logEntryGetAllUseCase;
        this._getAllByActionLogEntryIdUseCase = cradle.logEntryGetAllByActionLogEntryIdUseCase;
    }

    public all = async (req: Request, res: Response) => {
        var models = await this._getAllUseCase.execute();
        res.send(models);
    }

    public allByActionLogEntryId = async (req: Request, res: Response) => {
        var id = Number(req.params.id);
        var models = await this._getAllByActionLogEntryIdUseCase.execute(id);
        res.send(models);
    }

    public create = async (req: Request, res: Response) => {
        var dto = await LogEntryDto.create(req);
        
        var result = await this._createUseCase.execute(mapDtoToModel(dto));
        res.send(result);
    }
}