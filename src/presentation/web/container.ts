import { asClass, createContainer, asValue, AwilixContainer } from "awilix";
import { loadControllers, scopePerRequest } from "awilix-express";
import { Application } from "express";
import { LogEntryController } from "./controller/log-entry.controller";
import { getAppDataSource } from "../../infrastructure/database/datasource/datasource";
import { LogEntryRepository } from "../../infrastructure/database/repositories/log-entry.repository";
import { GetAllLogEntriesUseCase } from "../../application/usecases/log/get-all-log-entries.usecase";
import { CreateLogEntryUseCase } from "../../application/usecases/log/create-log-entry.usecase";
import { TicketRepository } from "../../infrastructure/filesystem/repository/ticket.repository";
import { TicketController } from "./controller/ticket.controller";
import { CreateTicketUseCase } from "../../application/usecases/ticket/create-ticket.usecase";
import { CreateActionLogEntryUseCase } from "../../application/usecases/log/create-action-log-entry.usecase";
import { ActionLogEntryRepository } from "../../infrastructure/database/repositories/action-log-entry.repository";
import { GetByIdActionLogEntryUseCase } from "../../application/usecases/log/get-by-id-action-log-entry.usecase";
import { ActionLogEntryController } from "./controller/action-log-entry.controller";
import { GetAllActionLogEntryUseCase } from "../../application/usecases/log/get-all-action-log-entries.usecase";
import { GetAllLogEntriesByActionLogEntryIdUseCase } from "../../application/usecases/log/get-all-log-entries-by-action-log-entry-id.usecase";

const dataSource = getAppDataSource();

export const loadContainer = (app: Application): void => {

  const container = createContainer({
    injectionMode: "PROXY"
  });
  
  container.register({
    dataSource: asValue(dataSource),
  });

  registerLogEntryServices(container);
  registerTicketServices(container);
  
  app.use(scopePerRequest(container));
  
  app.use(loadControllers('routes/**/*.routes.{ts,js}', {cwd: __dirname}));
}

const registerLogEntryServices = (container: AwilixContainer<any>): void => {
  container.register({
    actionLogEntryRepository: asClass(ActionLogEntryRepository).singleton(),
    actionLogEntryController: asClass(ActionLogEntryController).singleton(),
    actionLogEntryCreateUseCase:  asClass(CreateActionLogEntryUseCase).singleton(),
    actionLogEntryGetAllUseCase: asClass(GetAllActionLogEntryUseCase).singleton(),
    actionLogGetByIdUseCase: asClass(GetByIdActionLogEntryUseCase).singleton(),

    logEntryRepository: asClass(LogEntryRepository).singleton(),
    logEntryController: asClass(LogEntryController).singleton(),
    logEntryCreateUseCase: asClass(CreateLogEntryUseCase).singleton(),
    logEntryGetAllUseCase: asClass(GetAllLogEntriesUseCase).singleton(),
    logEntryGetAllByActionLogEntryIdUseCase: asClass(GetAllLogEntriesByActionLogEntryIdUseCase).singleton()
  });
};

const registerTicketServices = (container: AwilixContainer<any>): void => {
  container.register({
    ticketRepository: asClass(TicketRepository).singleton(),
    ticketController: asClass(TicketController).singleton(),
    ticketCreateUseCase: asClass(CreateTicketUseCase).singleton(),
  });
};