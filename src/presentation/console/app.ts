import { environment } from '../../environments/environment';
import { getAppDataSource } from '../../infrastructure/database/datasource/datasource';
import { serverLog } from '../../tools/logger';

import 'reflect-metadata' // needed for typeorm



serverLog.info('Starting...');

const initDatabase = () => {
  var datasource = getAppDataSource();
  datasource.initialize()
  .then(() => {
      serverLog.info('database connected');

  })
  .catch((error) => console.log(error))
}

initDatabase();
