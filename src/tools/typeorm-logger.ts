import { Logger, QueryRunner } from "typeorm";
import { serverLog } from "./logger";

export class TypeOrmLogger implements Logger {

    log(level: "log" | "info" | "warn", message: any, queryRunner?: QueryRunner): any {
      if (level === 'warn') {
        serverLog.error(message);
      }
      else {
        serverLog.info(message);
      }
    }

    logMigration(message: string, queryRunner?: QueryRunner): any {
      serverLog.info(message);
    }

    logQuery(query: string, parameters?: any[], queryRunner?: QueryRunner): any {
      serverLog.info(query, parameters);
    }

    logQueryError(error: string, query: string, parameters?: any[], queryRunner?: QueryRunner): any {
      serverLog.error(error, query, parameters);
    }

    logQuerySlow(time: number, query: string, parameters?: any[], queryRunner?: QueryRunner): any {
      serverLog.error(query, time, parameters);
    }

    logSchemaBuild(message: string, queryRunner?: QueryRunner): any {
      serverLog.info(message);
    }
}