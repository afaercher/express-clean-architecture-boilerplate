import *  as  winston  from  'winston';
import 'winston-daily-rotate-file';

const transportFileServerLog = new winston.transports.DailyRotateFile({
    level: 'info',
    filename: 'logs/server-%DATE%.log',
    datePattern: 'YYYY-MM-DD-HH',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '30d',
    format: winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss.SSS'}),
        winston.format.align(),
        winston.format.printf(info => `${[info.timestamp]} - ${info.level} - ${info.message}`),
    )
  });

  const transportFileServerLogError = new winston.transports.DailyRotateFile({
    level: 'error',
    filename: 'logs/server-%DATE%-error.log',
    datePattern: 'YYYY-MM-DD-HH',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '30d',
    format: winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss.SSS'}),
        winston.format.align(),
        winston.format.printf(error => `${[error.timestamp]} - ${error.level} - ${error.message}`),
    )
  });

const transportConsole = new winston.transports.Console({
    level: 'debug',
    format: winston.format.combine(
        winston.format.colorize({all:true}),
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss.SSS'}),
        winston.format.printf(info => `${[info.timestamp]} - ${info.level} - ${info.message}`),
    )
})

export const serverLog = winston.createLogger({
    transports: [transportFileServerLog, transportFileServerLogError, transportConsole]
})
