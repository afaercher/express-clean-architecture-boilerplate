import { LogEntry } from "../models/log-entry.model";
import { IGenericRepository } from "./base/generic-repository.interface";

export interface ILogEntryRepository extends IGenericRepository<LogEntry>{
  getAllByActionLogEntryId(id: number): Promise<LogEntry[]>
}