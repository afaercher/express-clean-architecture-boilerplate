import { ActionLogEntry } from "../models/action-log-entry.model";
import { IGenericRepository } from "./base/generic-repository.interface";

export interface IActionLogEntryRepository extends IGenericRepository<ActionLogEntry> {
    getById(id: number): Promise<ActionLogEntry>;
}