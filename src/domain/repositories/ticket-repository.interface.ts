import { Ticket } from "../models/ticket.model";
import { IGenericRepository } from "./base/generic-repository.interface";

export interface ITicketRepository extends IGenericRepository<Ticket>{
  
}