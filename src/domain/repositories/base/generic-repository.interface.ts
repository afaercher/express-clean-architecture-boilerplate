import { IBaseModel } from "../../models/base/base-model.interface";

export interface IGenericRepository<T extends IBaseModel> {
  getAll(): Promise<T[]>;
  get(id: object): Promise<T>;

  create(entity: T): Promise<T>
  update (entity: T): Promise<T>;
}