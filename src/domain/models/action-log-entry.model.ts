import { BaseModel } from "./base/base.model";

export class ActionLogEntry extends BaseModel {
  public uuid: string;
  public date: Date;
  public user: string;
  public service: string;
}