import { BaseModel } from "./base/base.model";

export class Ticket extends BaseModel {
  public pc: string;
  public validFrom: string;
  public validUntil: string;
  public duration: number;
  public reboots: number;
  public intrusionProtection: boolean;
  public autoReboot: boolean;
  public usbServiceMode: boolean;
  public disableTimeAutomationAlert: boolean;
  public issuer: string;

  public filepathSource: string;
  public filepathTarget: string;

  public constructor(
      pc: string,
      validFrom: string,
      validUntil: string,
      duration: number,
      reboots: number,
      intrusionProtection: boolean,
      autoReboot: boolean,
      usbServiceMode: boolean,
      disableTimeAutomationAlert: boolean,
      issuer: string
  ) {
    super();
    
    this.pc = pc;
    this.validFrom = validFrom;
    this.validUntil = validUntil;
    this.duration = duration;
    this.reboots = reboots;
    this.intrusionProtection = intrusionProtection;
    this.autoReboot = autoReboot;
    this.usbServiceMode = usbServiceMode;
    this.disableTimeAutomationAlert = disableTimeAutomationAlert;
    this.issuer = issuer;
  }
}
