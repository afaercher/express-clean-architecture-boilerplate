import { ActionLogEntry } from "./action-log-entry.model";
import { BaseModel } from "./base/base.model";

export class LogEntry extends BaseModel {
  public date: Date;
  public value: string;
  public level: string;
  public actionUuid: string;
  public actionLogEntry: ActionLogEntry;
}