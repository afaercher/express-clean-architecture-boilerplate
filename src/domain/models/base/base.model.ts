import { IBaseModel } from "./base-model.interface";

export abstract class BaseModel implements IBaseModel {
  public id: number;
}