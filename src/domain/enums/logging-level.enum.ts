export enum LoggingLevel {
    INFO = 'info',
    WARNING = 'warn',
    ERROR = 'error'
}