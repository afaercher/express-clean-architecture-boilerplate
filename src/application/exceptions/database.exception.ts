import { BaseException } from "./base/base.exception";

export class DatabaseException extends BaseException {

  public static Name = 'database exception';
  
  constructor(message: string, internalMessage: string) {
    super(DatabaseException.Name, message, internalMessage);
  }
}