export abstract class BaseException {

  public get name(): string { return this._name; }
  public get message(): string { return this._message; }
  public get internalMessage(): string { return this._internalMessage; }

  private _name: string;
  private _message: string;
  private _internalMessage: string;

  constructor(name: string, message: string, internalMessage: string) {
    this._name = name;
    this._message = message;
    this._internalMessage = internalMessage;
  }

  public toString(): string {
    return `Exception type: ${this.name} / Message : ${this.message} / Internal Message: ${this.internalMessage}`;
  }
}