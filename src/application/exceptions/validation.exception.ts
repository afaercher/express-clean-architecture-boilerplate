import { BaseException } from "./base/base.exception";

export class ValidationException extends BaseException {

  public static Name = 'validation exception';
  
  constructor(message: string, internalMessage: string) {
    super(ValidationException.Name, message, internalMessage);
  }
}