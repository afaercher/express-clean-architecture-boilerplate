import { LogEntry } from "../../../domain/models/log-entry.model";

export interface IGetAllLogEntriesByActionLogEntryId {
  execute(id: number): Promise<LogEntry[]>;
}