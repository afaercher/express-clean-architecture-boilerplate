import { LogEntry } from "../../../domain/models/log-entry.model";
import { IGenericGetAll } from "../base/generic-get-all.interface";

export interface IGetAllLogEntries extends IGenericGetAll<LogEntry> {
}