import { ActionLogEntry } from "../../../domain/models/action-log-entry.model";
import { IGenericGetAll } from "../base/generic-get-all.interface";

export interface IGetAllActionLogEntries extends IGenericGetAll<ActionLogEntry> {
}