import { ActionLogEntry } from "../../../domain/models/action-log-entry.model";
import { IGenericCreate } from "../base/generic-create.interface";

export interface ICreateActionLogEntry extends IGenericCreate<ActionLogEntry> {
}