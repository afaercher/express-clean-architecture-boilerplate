import { LogEntry } from "../../../domain/models/log-entry.model";
import { IGenericCreate } from "../base/generic-create.interface";

export interface ICreateLogEntry extends IGenericCreate<LogEntry> {
}