import { ActionLogEntry } from "../../../domain/models/action-log-entry.model";

export interface IGetByIDActionLogEntry {
  execute(id: number): Promise<ActionLogEntry>;
}