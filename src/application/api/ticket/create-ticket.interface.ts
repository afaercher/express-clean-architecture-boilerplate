import { Ticket } from "../../../domain/models/ticket.model";

export interface ICreateTicket {
  execute(model: Ticket): Promise<void>;
}