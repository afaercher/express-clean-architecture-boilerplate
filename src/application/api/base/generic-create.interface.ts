export interface IGenericCreate<T> {
  execute(model: T): Promise<T>;
}