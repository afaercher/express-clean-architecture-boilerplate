export interface IGenericGetAll<T> {
  execute(): Promise<T[]>;
}