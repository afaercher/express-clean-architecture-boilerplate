import { IGenericRepository } from "../../../domain/repositories/base/generic-repository.interface";

export abstract class GenericUseCase<T> {
  protected _repository: IGenericRepository<T>;

  constructor(repository) {
    this._repository = repository;
  }
}
