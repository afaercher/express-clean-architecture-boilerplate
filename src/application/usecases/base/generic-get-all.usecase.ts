import { IGenericGetAll } from "../../api/base/generic-get-all.interface";
import { GenericUseCase } from "./generic.usecase";

export abstract class GenericGetAllUseCase<T> extends GenericUseCase<T> implements IGenericGetAll<T> {
  public async execute(): Promise<T[]> {
    return this._repository.getAll();
  }
}
