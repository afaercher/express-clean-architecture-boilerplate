import { IGenericCreate } from "../../api/base/generic-create.interface";
import { GenericUseCase } from "./generic.usecase";

export abstract class GenericCreateUseCase<T> extends GenericUseCase<T> implements IGenericCreate<T> {
  public async execute(model: T): Promise<T> {
    return this._repository.create(model);
  }
}
