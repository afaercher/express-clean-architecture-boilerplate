import { ActionLogEntry } from "../../../domain/models/action-log-entry.model";
import { ICreateActionLogEntry } from "../../api/log/create-action-log-entry.interface";
import { GenericCreateUseCase } from "../base/generic-create.usecase";

export class CreateActionLogEntryUseCase extends GenericCreateUseCase<ActionLogEntry> implements ICreateActionLogEntry {
  constructor(cradle) {
    super(cradle.actionLogEntryRepository);
  }
}
