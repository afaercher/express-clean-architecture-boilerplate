import { IActionLogEntryRepository } from "../../../domain/repositories/action-log-entry-repository.interface";
import { IGetByIDActionLogEntry } from "../../api/log/get-by-id-action-log-entry.interface";
import { ActionLogEntry } from "../../../domain/models/action-log-entry.model";

export class GetByIdActionLogEntryUseCase implements IGetByIDActionLogEntry {
  private _repository: IActionLogEntryRepository;

  constructor(cradle) {
    this._repository = cradle.logActionEntryRepository;
  }

  public async execute(id: number): Promise<ActionLogEntry> {
    return this._repository.getById(id);
  }
}
