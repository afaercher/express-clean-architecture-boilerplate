import { IGetAllActionLogEntries } from "../../api/log/get-all-action-log-entries.interface";
import { ActionLogEntry } from "../../../domain/models/action-log-entry.model";
import { GenericGetAllUseCase } from "../base/generic-get-all.usecase";

export class GetAllActionLogEntryUseCase extends GenericGetAllUseCase<ActionLogEntry> implements IGetAllActionLogEntries {
  constructor(cradle) {
    super(cradle.actionLogEntryRepository);
  }
}
