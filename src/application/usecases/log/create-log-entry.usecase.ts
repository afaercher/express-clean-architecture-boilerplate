import { ICreateLogEntry } from "../../api/log/create-log-entry.interface";
import { LogEntry } from "../../../domain/models/log-entry.model";
import { GenericCreateUseCase } from "../base/generic-create.usecase";

export class CreateLogEntryUseCase extends GenericCreateUseCase<LogEntry> implements ICreateLogEntry {
  constructor(cradle) {
    super(cradle.logEntryRepository);
  }
}
