import { IGetAllLogEntries } from "../../api/log/get-all-log-entries.interface";
import { LogEntry } from "../../../domain/models/log-entry.model";
import { GenericGetAllUseCase } from "../base/generic-get-all.usecase";

export class GetAllLogEntriesUseCase extends GenericGetAllUseCase<LogEntry> implements IGetAllLogEntries {
  constructor(cradle) {
    super(cradle.logEntryRepository);
  }
}
