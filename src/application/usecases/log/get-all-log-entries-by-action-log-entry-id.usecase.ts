import { LogEntry } from "../../../domain/models/log-entry.model";
import { ILogEntryRepository } from "../../../domain/repositories/log-entry-repository.interface";
import { IGetAllLogEntriesByActionLogEntryId } from "../../api/log/get-all-log-entries-by-action-log-entry-id.interface";

export class GetAllLogEntriesByActionLogEntryIdUseCase implements IGetAllLogEntriesByActionLogEntryId {

  private _repository: ILogEntryRepository;

  constructor(cradle) {
    this._repository = cradle.logEntryRepository;
  }

  public async execute(id: number): Promise<LogEntry[]> {
    return this._repository.getAllByActionLogEntryId(id);
  }
}
