import { ICreateTicket } from "../../api/ticket/create-ticket.interface";
import { Ticket } from "../../../domain/models/ticket.model";
import { ITicketRepository } from "../../../domain/repositories/ticket-repository.interface";

export class CreateTicketUseCase implements ICreateTicket {
  private _repository: ITicketRepository;

  constructor(cradle) {
    this._repository = cradle.ticketRepository;
  }

  public async execute(model: Ticket): Promise<void> {
    await this._repository.create(model);
  }
}
