import { ITicketRepository } from "../../../domain/repositories/ticket-repository.interface";
import { Ticket } from "../../../domain/models/ticket.model";
import { tmpdir } from 'os';
import { randomBytes } from 'crypto';
import { join } from 'path';
import { hostname } from 'os';
import { spawn } from 'child_process';
import { environment } from '../../../environments/environment';
import * as fs from 'fs';
import { serverLog } from "../../../tools/logger";


export class TicketRepository implements ITicketRepository{
  constructor() {
  }

  get(id: object): Promise<Ticket> {
    throw new Error("Method not allowed.");
  }

  public async getAll(): Promise<Ticket[]> {
    throw new Error("Method not allowed.");
  }
  
  async create(model: Ticket): Promise<Ticket> {
    return new Promise((resolve, reject) => {
      const ticketFileContent = TicketRepository._content(model);

      const randomValue = (randomBytes(20).toString('hex'));
      const filenameSource = 'ticket-source_'+randomValue;
      const filenameTarget = 'ticket-target_'+randomValue;
      model.filepathSource = join(tmpdir(), filenameSource);
      model.filepathTarget = join(tmpdir(), filenameTarget);
  
      fs.writeFileSync(model.filepathSource, ticketFileContent);
  
      const cmd = spawn(environment.tssign, ['TICKET', '-s', model.filepathSource, '-t', model.filepathTarget, '-k', environment.privateKey, '-i', model.issuer]);
  
      cmd.stdout.on('data', data => serverLog.debug(data.toString()));
      cmd.stderr.on('data', data => serverLog.error(data.toString()));
  
      cmd.on('close', (exitCode) => {
          if (exitCode !== 0) {
              serverLog.error('command failed with rc ' + exitCode);
              //res.status(400).send('failed to execute command');
              throw 'command failed with rc ' + exitCode;
          }

          resolve(model);
      });
    });
  }
  
  update(entity: Ticket): Promise<Ticket> {
    throw new Error("Method not allowed.");
  }

  private static _content(ticket: Ticket): string {
    const timestamp = Math.floor(new Date().getTime() / 1000);
    const ipOffvalue = 'IntrusionProtection';

    return 'version=3.0\n'+
    `products=${ipOffvalue}\n`+
    `computername=${ticket.pc}\n`+
    `validfrom=${ticket.validFrom}\n`+
    `validto=${ticket.validUntil}\n`+
    `duration=${ticket.duration}\n`+
    `reboots=${ticket.reboots}\n`+
    `issuer=${hostname}:${ticket.issuer}\n`+
    `autologon_user=\n`+
    `auto_reboot=${ticket.autoReboot}\n`+
    `timestamp=${timestamp}\n`+
    `usb_service_mode=${ticket.usbServiceMode}\n`+
    `unblock_time_manipulation=${ticket.disableTimeAutomationAlert}`;
  }
}