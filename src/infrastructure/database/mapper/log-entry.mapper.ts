import { LogEntry } from "../../../domain/models/log-entry.model";
import { LogEntryEntity } from "../entities/logger/log-entry.entity";
import { IBaseMapper } from "./base/base-mapper.interface";
import { BaseMapper } from "./base/base.mapper";
import { ActionLogEntryMapper } from "./action-log-entry.mapper";

export class LogEntryMapper extends BaseMapper<LogEntry, LogEntryEntity> implements IBaseMapper<LogEntry, LogEntryEntity>{

  private _actionLogEntryMapper: ActionLogEntryMapper;

  constructor() {
    super();
    this._actionLogEntryMapper = new ActionLogEntryMapper();
  }
    
  public mapEntityToModel(entity: LogEntryEntity): LogEntry {
    var model = new LogEntry();
    model.id = entity.id;
    model.level = entity.level;
    model.date = entity.date;
    model.value = entity.value;
    model.actionUuid = entity.actionUuid;

    if (entity.actionLogEntry) {
      model.actionLogEntry = this._actionLogEntryMapper.mapEntityToModel(entity.actionLogEntry);
    }
    return model;
  }

  public mapModelToEntity(model: LogEntry): LogEntryEntity {
    console.log(model)
    var entity = new LogEntryEntity();
    entity.id = model.id;
    entity.level = model.level;
    entity.date = model.date;
    entity.value = model.value;
    entity.actionUuid = model.actionUuid;
    //Commented because ActionLogEntry could be later created than LogEntries 
    /* if (model.actionLogEntry) {
      entity.actionLogEntry = this._actionLogEntryMapper.mapModelToEntity(model.actionLogEntry);
    } */
    return entity;
  }
}