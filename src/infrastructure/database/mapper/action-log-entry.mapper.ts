import { ActionLogEntryEntity } from "../entities/logger/action-log-entry.entity";
import { IBaseMapper } from "./base/base-mapper.interface";
import { BaseMapper } from "./base/base.mapper";
import { ActionLogEntry } from "../../../domain/models/action-log-entry.model";

export class ActionLogEntryMapper extends BaseMapper<ActionLogEntry, ActionLogEntryEntity> implements IBaseMapper<ActionLogEntry, ActionLogEntryEntity>{
  mapEntityToModel(entity: ActionLogEntryEntity): ActionLogEntryEntity {
    var model = new ActionLogEntry();
    model.id = entity.id;
    model.date = entity.date;
    model.service = entity.service;
    model.user = entity.user;
    model.uuid = entity.uuid;
    return model;
  }
  
  mapModelToEntity(model: ActionLogEntry): ActionLogEntryEntity {
    var entity = new ActionLogEntryEntity();
    entity.id = model.id;
    entity.date = model.date;
    entity.service = model.service;
    entity.user = model.user;
    entity.uuid = model.uuid;
    return entity;
  }
}