import { BaseDatabaseEntity } from "../../entities/base/base-database.entity";
import { IBaseMapper } from "./base-mapper.interface";
import { BaseModel } from "../../../../domain/models/base/base.model";

export abstract class BaseMapper<TModel extends BaseModel, TEntity extends BaseDatabaseEntity> implements IBaseMapper<TModel, TEntity>{
  public mapEntitiesToModels(entities: TEntity[]): TModel[]{
    if (!entities) {
      return [];
    }

    var models = [];

    entities.forEach((entity) => {
      var model = this.mapEntityToModel(entity);
      models.push(model);
    });
    
    return models;
  }

  abstract mapEntityToModel(entity: TEntity): TModel;
  
  public mapModelsToEntities(models: TModel[]): TEntity[]{
    if (!models) {
      return [];
    }

    var entities = [];

    models.forEach((model) => {
      var entity = this.mapModelToEntity(model);
      entities.push(entity);
    })
  
    return entities;
  }

  abstract mapModelToEntity(model: TModel): TEntity;
}