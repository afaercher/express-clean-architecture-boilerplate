import { BaseModel } from "../../../../domain/models/base/base.model";
import { BaseDatabaseEntity } from "../../entities/base/base-database.entity";

export interface IBaseMapper<TModel extends BaseModel, TEntity extends BaseDatabaseEntity>{
  mapEntitiesToModels(entities: TEntity[]): TModel[] 
  mapEntityToModel(entity: TEntity): TModel;
  
  mapModelsToEntities(models: TModel[]): TEntity[];
  mapModelToEntity(model: TModel): TEntity;
}