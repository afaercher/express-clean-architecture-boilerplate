import { LogEntryEntity } from "../entities/logger/log-entry.entity";
import { LogEntry } from "../../../domain/models/log-entry.model";
import { ILogEntryRepository } from "../../../domain/repositories/log-entry-repository.interface";
import { GenericDatabaseRepository } from "./base/generic-database.repository";
import { LogEntryMapper } from "../mapper/log-entry.mapper";

export class LogEntryRepository extends GenericDatabaseRepository<LogEntry, LogEntryEntity> implements ILogEntryRepository{
  
  constructor(cradle) {
    super(cradle.dataSource, cradle.dataSource.getRepository(LogEntryEntity), new LogEntryMapper());
  }
  
  public async getAll(): Promise<LogEntry[]> {  
    const query = this._repository.createQueryBuilder("logEntryEntity")
      .leftJoinAndSelect('logEntryEntity.actionLogEntry', 'ale')
    const entities =  await query.getMany();
    return this._mapper.mapEntitiesToModels(entities);
  }

  public async getAllByActionLogEntryId(id: number): Promise<LogEntry[]> {
    const entity = await this._repository.find({
      where: {
        actionLogEntry: {
          id: id,
        }
      },
    });
    return this._mapper.mapEntitiesToModels(entity);
  }
}