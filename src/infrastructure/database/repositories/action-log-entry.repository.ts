import { IActionLogEntryRepository } from "../../../domain/repositories/action-log-entry-repository.interface";
import { ActionLogEntryEntity } from "../entities/logger/action-log-entry.entity";
import { ActionLogEntry } from "../../../domain/models/action-log-entry.model";
import { GenericDatabaseRepository } from "./base/generic-database.repository";
import { ActionLogEntryMapper } from "../mapper/action-log-entry.mapper";

export class ActionLogEntryRepository extends GenericDatabaseRepository<ActionLogEntry, ActionLogEntryEntity> implements IActionLogEntryRepository{
  
  constructor(cradle) {
    super(cradle.dataSource, cradle.dataSource.getRepository(ActionLogEntryEntity), new ActionLogEntryMapper());
  }

  async getById(id: number): Promise<ActionLogEntry> {
    const entity = await this._repository.findOne({
        where: {
            id: id,
        },
    })
    return this._mapper.mapEntityToModel(entity);
  }
}