import { IGenericRepository } from "../../../../domain/repositories/base/generic-repository.interface";
import { BaseDatabaseEntity } from "../../entities/base/base-database.entity";
import { BaseModel } from "../../../../domain/models/base/base.model";

export interface IGenericDatabaseRepository<TModel extends BaseModel, TEntity extends BaseDatabaseEntity> extends IGenericRepository<TModel>{
  
  // async getById(id: number): Promise<T> {
  //   const entity = await this._repository.findOne({
  //       where: {
  //           id: id,
  //       },
  //   })
  //   return mapEntityToModel(entity);
  // }

  get(id: object): Promise<TModel>;

  getAll(): Promise<TModel[]>;

  create(model: TModel): Promise<TModel>;

  update(model: TModel): Promise<TModel>;
}

// TODO: https://stackoverflow.com/questions/38708550/difference-between-return-await-promise-and-return-promise/42750371#42750371