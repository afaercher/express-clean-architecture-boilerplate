import { DataSource, Repository } from "typeorm";
import { BaseDatabaseEntity } from "../../entities/base/base-database.entity";
import { BaseModel } from "../../../../domain/models/base/base.model";
import { IBaseMapper } from "../../mapper/base/base-mapper.interface";
import { IGenericDatabaseRepository } from "./generic-database-repository.interface";
import { DatabaseException } from "../../../../application/exceptions/database.exception";

export abstract class GenericDatabaseRepository<TModel extends BaseModel, TEntity extends BaseDatabaseEntity> implements IGenericDatabaseRepository<TModel, TEntity>{
  
  protected _dataSource: DataSource;
  protected _repository: Repository<TEntity>;
  protected _mapper: IBaseMapper<TModel, TEntity>;

  constructor(dataSource: DataSource, repository: Repository<TEntity>, mapper: IBaseMapper<TModel, TEntity>) {
    this._dataSource = dataSource;
    this._repository = repository;
    this._mapper = mapper;
  }

  // public async getById(id: object): Promise<TModel> {
  //   const entity = await this._repository.findOne({
  //     where: {
  //       id: id
  //     }
  //       // where: {
  //       //     id: id,
  //       // },
  //   })
  //   return this._mapper.mapEntityToModel(entity);
  // }

  public async get(id: object): Promise<TModel> {
      throw new Error("Method not implemented.");
  }

  public async getAll(): Promise<TModel[]> {
    var entities = await this._repository.find();
    return this._mapper.mapEntitiesToModels(entities);
  }

  public async create(model: TModel): Promise<TModel> {
    var entity = this._mapper.mapModelToEntity(model);

    try {
      await this._repository.save(entity); 
    } catch (error) {
      throw new DatabaseException('Save Error', error);
    }

    model.id = entity.id;
    return model;
  }
  
  public async update(model: TModel): Promise<TModel> {
    throw new Error("Method not implemented.");
  }
}