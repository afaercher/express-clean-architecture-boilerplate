import { DataSource } from "typeorm";
import { environment } from "../../../environments/environment";
import { DatabaseException } from "../../../application/exceptions/database.exception";
import path from "path";
import { TypeOrmLogger } from "../../../tools/typeorm-logger";

const _entitydir = path.join(__dirname, '..', 'entities');
const _entities = [_entitydir + "/**/*.entity.{ts,js}"]

const _entityPrefix = 'AUTOMATION_';

export const getAppDataSource = (): DataSource => {
  switch (environment.database_type) {
    case "sqljs":
      return sqljsDataSource;
    case "oracle":
      return oraclejsDataSource;
    default:
      throw new DatabaseException('Invalid database configuration', environment.database_type);
  } 
}

const sqljsDataSource = new DataSource({
  type: "sqljs" as "sqljs",
  synchronize: true,
  logging: 'all',
  logger: new TypeOrmLogger(),
  entities: _entities,
  subscribers: [],
  migrations: [],
  entityPrefix: _entityPrefix
});

const oraclejsDataSource = new DataSource({
  type: "oracle" as "oracle",
  host: environment.database_host,
  port: environment.database_port,
  username: environment.database_username,
  password: environment.database_password,
  serviceName: environment.database_serviceName,
  synchronize: false,
  logging: true,
  logger: new TypeOrmLogger(),
  // entities: _entities,
  entities: [],
  subscribers: [],
  migrations: [],
  entityPrefix: _entityPrefix
});


// todo: custom logger using winston
// https://typeorm.io/logging#   ||   https://stackoverflow.com/questions/56988392/how-to-use-winston-with-typeorm