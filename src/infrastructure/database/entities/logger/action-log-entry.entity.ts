import { Entity, Column, CreateDateColumn } from "typeorm";
import { BaseDatabaseEntity } from "../base/base-database.entity";

@Entity()
export class ActionLogEntryEntity extends BaseDatabaseEntity{
  @CreateDateColumn()
  public date: Date;

  @Column({
    type: 'uuid',
    unique: true
  })
  public uuid: string;

  @Column()
  public user: string;

  @Column()
  public service: string;
}