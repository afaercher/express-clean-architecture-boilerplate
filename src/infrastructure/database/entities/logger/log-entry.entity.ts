import { Entity, Column, CreateDateColumn, ManyToOne, JoinColumn } from "typeorm";
import { ActionLogEntryEntity } from "./action-log-entry.entity";
import { BaseDatabaseEntity } from "../base/base-database.entity";

@Entity()
export class LogEntryEntity extends BaseDatabaseEntity{

  @CreateDateColumn()
  public date: Date;

  @Column({
    default: "Info"
  })
  public level: string

  @Column()
  public value: string;

  @Column({
    type: 'uuid',
  })
  public actionUuid: string;
  
  @ManyToOne(() => ActionLogEntryEntity, {
    eager: true,
    nullable: true,
    createForeignKeyConstraints: false,
  })
  @JoinColumn({
    name: 'actionUuid',
    referencedColumnName: 'uuid',
  })
  public actionLogEntry: ActionLogEntryEntity
}