import { PrimaryGeneratedColumn } from "typeorm";
import { IBaseDatabaseEntity } from "./base-database-entity.interface";

export abstract class BaseDatabaseEntity implements IBaseDatabaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;
}